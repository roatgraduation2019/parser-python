#!/usr/bin/env python
# -*- coding: cp1251 -*-

import os
import re
import pandas as pd
import json
from collections import defaultdict
from itertools import groupby
import pyperclip

def date(a):
    re.sub(r"([аА-яЯ])", "", a)
	
def to_js(a):
    with open('data.json', 'w', encoding='utf-8') as file:
        a.to_json(path_or_buf=file,orient='records', force_ascii=False)

def to_pd(html):
    tables = pd.read_html(html)
    t = tables
    name = tables[0][2][2]#сохраняет имя преподователя из файла
    del t[0]
    for i in range(len(t)):
        #удаляет колонки:№,номер подгр.,преподователь
        t[i] = t[i].drop(columns=[0,5,12])
        #удаляет первый DataFrame,он не нужен для бдю
        t[i] = t[i].drop([0])
    names = ['date','starttime','endtime','type','discipline','NoStud','auditory','kurs','group','specialty']
    tt = pd.concat(t)# обьединяет таблицы в одну
    tt = tt.reset_index(drop=True)
    tt[1] = (tt[1].str.replace(r'[аА-яЯ]',''))
        #.str.replace('.','-'))
    tt.columns = names
    tt['Prepod'] = name
    return tt

def frame_to_dict(df):
	i = 0
	ii = 0
	d = {'Professors':[]}
	for p in df.groupby(['Prepod']):
		d['Professors'].append({'name':p[0],'dates':[]})
		for dd in p[1].groupby(['date']):
			d['Professors'][i]['dates'].append({'date':dd[0],'lessons':[]})
			for line in dd[1].values:
				ddd = {'start_time':line[1],'end_time':line[2],'type':line[3],'lesson':line[4],'stud_count':line[5],'kurs':line[7],'group':line[8],'speciality':line[9]}
				d['Professors'][i]['dates'][ii]['lessons'].append(ddd)
			ii+= 1  
		i+=1
		ii=0
	i = 0
	return d
	

path =os.getcwd()+'\\data\\'
flist = [u'ВеселоваАС.htm', 'ВолковАС.htm', 'ГоворовЮВ.htm', 'ГореликАВ.htm', 'ГореликВЮ.htm', 'ГусароваЕВ.htm', 'ДороховВС.htm', 'ЕрмаковАЕ.htm', 'ЕрмаковаОП.htm', 'ЖуравлевИА.htm', 'КнышевИП.htm', 'КоптеваЛГ.htm', 'МалыхАН.htm', 'НеваровПА.htm', 'ОрловАВ.htm', 'ПолонскийАВ.htm', 'РидельВВ.htm', 'СёмочкинЕВ.htm', 'СавченкоПВ.htm', 'СаммеГВ.htm', 'СперанскийДВ.htm', 'ТарадинНА.htm']
fpath = path+flist[0]
html = open(path+flist[0],encoding="cp1251").read()

tt = to_pd(html)
aa = pd.core.frame.DataFrame(data=None)

for i in range(len(flist)):
    a = to_pd(open(path+flist[i],encoding="cp1251").read())
    aa = pd.concat([aa,a])
    aa.reset_index(drop=True)


#js = aa.to_json(orient='records', force_ascii=False)
#js = json.loads(js)
# j = tt.to_json(orient='records', force_ascii=False)
# jj = aa.groupby(['Prepod'])['date','starttime','endtime','type','discipline','NoStud','auditory','kurs','group','specialty']
# jjj = jj.apply(lambda x: x.to_json(orient='records',force_ascii=False))

a = str(frame_to_dict(aa))
with open('data.json', 'w', encoding='utf-8') as file:
    file.write(a)
    
##    aa.to_json(path_or_buf=file,orient='records', force_ascii=False)

pyperclip.copy(str(frame_to_dict(aa)))
