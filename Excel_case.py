#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import numpy as np
import pandas as pd


def to_df(excel):
    df = pd.read_excel(excel,
                       index_col=None,
                       names =['No', 'date', 'start_time', 'end_time',
                               'type', 'lesson', 'stud_count',
                               'auditory', 'kurs', 'group',
                               'subgroup', 'professor'])
    wrong = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'Дата']
    for item in wrong:
        df = df[df.date != item]
    replace_nan_rule = {
        'auditory': '',
        'kurs': 0,
        'group': '',
        'subgroup': ''
    }
    df = df.fillna(value=replace_nan_rule)
    df['group'] = df['group'] + df['subgroup']
    df = df.drop(columns=['No', 'subgroup'])
    return df


def get_lessons(data):
    lessons = list()
    for lesson in data.iterrows():
        lessons.append(lesson[1].to_dict())
    return lessons


def get_group_by_date(data):
    group = dict()
    for date_group in data.groupby(['date']):
        group[date_group[0]] = get_group_by_professor(date_group[1])
    return group


def get_group_by_professor(data):
    group = dict()
    for professor_group in data.groupby(['professor']):
        group[professor_group[0]] = get_lessons(professor_group[1])
    return group


def frame_to_dict(dataframe):
    return get_group_by_date(dataframe)


def excel_to_json(fpath):
    df = to_df(fpath)
    return frame_to_dict(df)


if __name__ == "__main__":
    print(excel_to_json())
