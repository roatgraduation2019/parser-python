import os
from flask import Flask, request, jsonify
import Excel_case


app = Flask(__name__)
port = int(os.environ.get('$PORT',5000))


@app.route('/')
def upload_file():
    return '''
<html>
   <body>
      <form action = "/json" method = "POST"
         enctype = "multipart/form-data">
         <input type = "file" name = "file" />
         <input type = "submit" value = "Upload"/>
      </form>
   </body>
</html>'''


@app.route('/json', methods = ['POST'])
def upload_file_1():
   if request.method == 'POST':
      f = request.files['file']
      return jsonify(Excel_case.excel_to_json(f))

if __name__ == '__main__':
    app.run('0.0.0.0', port)
